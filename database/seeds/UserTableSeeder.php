<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'email' => 'tester@test.com',
            'password' => app('hash')->make('secret')
        ]);

        factory(App\User::class)->create([
            'email' => 'tester2@test.com',
            'password' => app('hash')->make('secret')
        ]);

        factory(App\User::class)->create([
            'email' => 'tester3@test.com',
            'password' => app('hash')->make('secret')
        ]);
    }
}