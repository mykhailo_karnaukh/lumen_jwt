<?php

$app->POST('/auth/login', 'AuthController@loginPost');
$app->group(['prefix' => 'api', 'middleware' => 'jwt.auth'], function($app) {
	$app->GET('/users', 'AuthController@users');
});